package com.i108.conferenceapp.fragments;

import java.util.List;

import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.ConferenceApp;
import com.i108.conferenceapp.adapter.RSSListAdapter;

import android.support.v4.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class NewsRSSFragment extends Fragment {

	private static final String RSS_URI = "http://feeds.bbci.co.uk/news/world/rss.xml";

	private List<RSSItem> data;
	private RSSListAdapter adapter;
	private ListView list;
	private ProgressBar progressBar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_rss, container, false);
		list = (ListView) rootView.findViewById(R.id.list);
		list.setSelector(android.R.color.transparent);
		progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

		if (ConferenceApp.isNetworkAvailable(getActivity())) {
			new GetRSSNewsAsyncTask().execute();
		} else {
			progressBar.setVisibility(View.GONE);
			Toast.makeText(getActivity(), R.string.network_unavailable, Toast.LENGTH_LONG).show();
		}
		return rootView;
	}

	private void refresh() {
		progressBar.setVisibility(View.GONE);
		if (data != null && getActivity() != null) {
			adapter = new RSSListAdapter(getActivity(), data);
			list.setAdapter(adapter);
		}
	}

	private class GetRSSNewsAsyncTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			RSSReader reader = new RSSReader();
			try {
				RSSFeed feed = reader.load(RSS_URI);
				data = feed.getItems();
			} catch (RSSReaderException e) {
				return e.getMessage();
			} finally {
				reader.close();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result == null)
				refresh();
			else if (getActivity() != null)
				Toast.makeText(getActivity(), R.string.rss_error, Toast.LENGTH_LONG).show();
		}

	}

}
