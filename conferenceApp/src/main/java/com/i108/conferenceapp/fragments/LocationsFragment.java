package com.i108.conferenceapp.fragments;

import java.util.ArrayList;
import java.util.List;

import com.i108.conferenceapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.i108.conferenceapp.ConferenceApp;
import com.i108.conferenceapp.model.Place;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class LocationsFragment extends Fragment {

	public static final String PLACE_ID = "placeId";

	private ConferenceApp app;
	private GoogleMap map;
	private SupportMapFragment mapFragment;
	private List<Place> places;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_locations, container, false);
		app = (ConferenceApp) getActivity().getApplication();

		mapFragment = SupportMapFragment.newInstance();
		getChildFragmentManager().beginTransaction().replace(R.id.map_layout, mapFragment).commit();

		long id = getArguments() == null ? -1 : getArguments().getLong(PLACE_ID, -1);
		if (id < 0)
			places = app.getDbManager().getAllPlaces();
		else {
			places = new ArrayList<Place>();
			places.add(app.getDbManager().getPlace(id));
		}

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO: change this code to use getMapAsync()
//		if (map == null) {
//			map = mapFragment.getMap();
//			prepareMap();
//		}
		super.onResume();
	}

//	private void prepareMap() {
//		map.setMyLocationEnabled(true);
//		map.moveCamera(CameraUpdateFactory.newLatLngZoom(places.get(0).getPosition(), 13));
//		for (int i = 0; i < places.size(); i++) {
//			map.addMarker(new MarkerOptions().title(places.get(i).getName()).position(places.get(i).getPosition()));
//		}
//	}

}
