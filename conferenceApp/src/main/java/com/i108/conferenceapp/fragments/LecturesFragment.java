package com.i108.conferenceapp.fragments;

import java.util.List;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.ConferenceApp;
import com.i108.conferenceapp.activities.MainActivity;
import com.i108.conferenceapp.activities.details.LectureActivity;
import com.i108.conferenceapp.adapter.LecturesListAdapter;
import com.i108.conferenceapp.model.Lecture;
import com.i108.conferenceapp.utils.Utils;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class LecturesFragment extends Fragment {

	public static final String DAY_TAG = "day_tag";

	private ConferenceApp app;
	private MainActivity activity;
	private ListView list;
	private List<Lecture> data;
	private LecturesListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.list, container, false);
		activity = (MainActivity) getActivity();
		app = (ConferenceApp) activity.getApplication();

		list = (ListView) rootView.findViewById(R.id.list);
		data = app.getDbManager().getLecturesForDay(Utils.stringToDate(getArguments().getString(DAY_TAG)));
		adapter = new LecturesListAdapter(getActivity(), data, false);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				showDetails(id);
			}
		});
		if (activity.isDualPane() && !data.isEmpty())
			showDetails(data.get(0).getId());

		return rootView;
	}

	private void showDetails(long lectureId) {
		Bundle args = new Bundle();
		args.putLong(LectureDetailsFragment.LECTURE_ID, lectureId);
		if (activity.isDualPane()) {
			Fragment fragment = new LectureDetailsFragment();
			fragment.setArguments(args);
			activity.setDetails(fragment);
		} else {
			LectureActivity.startActivity(activity, args);
		}
	}

}
