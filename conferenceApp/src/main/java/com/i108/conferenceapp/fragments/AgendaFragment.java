package com.i108.conferenceapp.fragments;

import java.util.List;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.ConferenceApp;
import com.i108.conferenceapp.adapter.LecturesPagerAdapter;
import com.i108.conferenceapp.model.DayTag;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AgendaFragment extends Fragment {

	private ConferenceApp app;
	private ViewPager pager;
	private List<DayTag> data;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.pager, container, false);
		app = (ConferenceApp) getActivity().getApplication();

		pager = (ViewPager) rootView.findViewById(R.id.pager);
		data = app.getDbManager().getDayTags();
		pager.setAdapter(new LecturesPagerAdapter(getChildFragmentManager(), data));

		return rootView;
	}

}
