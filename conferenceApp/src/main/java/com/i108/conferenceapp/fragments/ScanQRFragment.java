package com.i108.conferenceapp.fragments;

import com.i108.conferenceapp.R;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener;
import com.i108.conferenceapp.utils.Utils;

import android.support.v4.app.Fragment;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class ScanQRFragment extends Fragment {

	private QRCodeReaderView readerView;
	private ImageView ivLine;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_scan_qr, container, false);

		ivLine = (ImageView) rootView.findViewById(R.id.iv_line);
		readerView = (QRCodeReaderView) rootView.findViewById(R.id.qrdecoderview);
		readerView.setOnQRCodeReadListener(new OnQRCodeReadListener() {
			@Override
			public void onQRCodeRead(String text, PointF[] points) {
				readerView.getCameraManager().stopPreview();
				Utils.openUrl(getActivity(), text);
			}

			@Override
			public void cameraNotFound() {
				// TODO Auto-generated method stub
			}

			@Override
			public void QRCodeNotFoundOnCamImage() {
				// TODO Auto-generated method stub
			}
		});

		TranslateAnimation animation = new TranslateAnimation(TranslateAnimation.ABSOLUTE, 0f,
				TranslateAnimation.ABSOLUTE, 0f, TranslateAnimation.RELATIVE_TO_PARENT, 0f,
				TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
		animation.setDuration(1000);
		animation.setRepeatCount(-1);
		animation.setRepeatMode(Animation.REVERSE);
		animation.setInterpolator(new LinearInterpolator());
		ivLine.setAnimation(animation);

		return rootView;
	}

	@Override
	public void onResume() {
		readerView.getCameraManager().startPreview();
		super.onResume();
	}

	@Override
	public void onPause() {
		readerView.getCameraManager().stopPreview();
		super.onPause();
	}

}
