package com.i108.conferenceapp.activities.details;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.fragments.LocationsFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;

public class LocationsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_details);

		Fragment fragment = new LocationsFragment();
		fragment.setArguments(getIntent().getExtras());
		getSupportFragmentManager().beginTransaction().replace(R.id.details, fragment).commit();
	}

	public static void startActivity(Context context, Bundle args) {
		Intent intent = new Intent(context, LocationsActivity.class);
		intent.putExtras(args);
		context.startActivity(intent);
	}

}
