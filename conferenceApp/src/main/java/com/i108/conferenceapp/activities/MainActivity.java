package com.i108.conferenceapp.activities;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.adapter.NavigationListAdapter;
import com.i108.conferenceapp.constants.NavigationPositions;
import com.i108.conferenceapp.fragments.AboutConferenceFragment;
import com.i108.conferenceapp.fragments.AgendaFragment;
import com.i108.conferenceapp.fragments.LocationsFragment;
import com.i108.conferenceapp.fragments.LogoFragment;
import com.i108.conferenceapp.fragments.MyPlanFragment;
import com.i108.conferenceapp.fragments.NewsRSSFragment;
import com.i108.conferenceapp.fragments.ScanQRFragment;
import com.i108.conferenceapp.fragments.SpeakersFragment;
import com.i108.conferenceapp.fragments.SponsorsFragment;
import com.i108.conferenceapp.fragments.TwitterFeedFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {

	private DrawerLayout drawer;
	private ListView drawerList;
	private ActionBarDrawerToggle drawerToggle;

	private String[] navigationTitles;
	private CharSequence drawerTitle;
	private CharSequence title;

	private boolean isDualPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		title = drawerTitle = getTitle();

		drawer = (DrawerLayout) findViewById(R.id.drawer);
		drawerList = (ListView) findViewById(R.id.drawer_list);

		navigationTitles = getResources().getStringArray(R.array.navigation_items);
		drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.drawer_open, R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getSupportActionBar().setTitle(title);
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle(drawerTitle);
				getSupportActionBar().setIcon(R.drawable.ic_launcher);
				invalidateOptionsMenu();
			}
		};
		drawer.setDrawerListener(drawerToggle);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		drawerList.addHeaderView(getLayoutInflater().inflate(R.layout.menu_header, drawerList, false));
		drawerList.setAdapter(new NavigationListAdapter(this, navigationTitles));
		drawerList.setOnItemClickListener(new NavigationClickListener());
		drawerList.performItemClick(null, NavigationPositions.AGENDA,
				drawerList.getAdapter().getItemId(NavigationPositions.AGENDA));

		isDualPane = findViewById(R.id.details) != null;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item))
			return true;
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// boolean drawerOpen = drawer.isDrawerOpen(drawerList);
		// menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	private void setContent(Fragment fragment) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
	}

	public void setDetails(Fragment fragment) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.details, fragment).commit();
	}

	@Override
	public void setTitle(CharSequence title) {
		this.title = title;
		getSupportActionBar().setTitle(this.title);
	}

	private class NavigationClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Fragment content = null;
			Fragment details = null;
			if (position == 0)
				return;
			switch (position) {
			case NavigationPositions.NEWS_RSS:
				if (isDualPane()) {
					content = new LogoFragment();
					details = new NewsRSSFragment();
				} else
					content = new NewsRSSFragment();
				break;
			case NavigationPositions.TWITTER_FEED:
				if (isDualPane()) {
					content = new LogoFragment();
					details = new TwitterFeedFragment();
				} else
					content = new TwitterFeedFragment();
				break;
			case NavigationPositions.AGENDA:
				content = new AgendaFragment();
				break;
			case NavigationPositions.MY_PLAN:
				content = new MyPlanFragment();
				break;
			case NavigationPositions.SPEAKERS:
				content = new SpeakersFragment();
				break;
			case NavigationPositions.SPONSORS:
				if (isDualPane()) {
					content = new LogoFragment();
					details = new SponsorsFragment();
				} else
					content = new SponsorsFragment();
				break;
			case NavigationPositions.LOCATIONS:
				if (isDualPane()) {
					content = new LogoFragment();
					details = new LocationsFragment();
				} else
					content = new LocationsFragment();
				break;
			case NavigationPositions.SCAN_QR_CODE:
				if (isDualPane()) {
					content = new LogoFragment();
					details = new ScanQRFragment();
				} else
					content = new ScanQRFragment();
				break;
			case NavigationPositions.ABOUT_CONFERENCE:
				if (isDualPane()) {
					content = new LogoFragment();
					details = new AboutConferenceFragment();
				} else
					content = new AboutConferenceFragment();
				break;
			}
			setContent(content);
			if (details != null)
				setDetails(details);

			drawerList.setItemChecked(position, true);
			setTitle(navigationTitles[position - 1]);
			drawer.closeDrawer(drawerList);
		}
	}

	public boolean isDualPane() {
		return isDualPane;
	}

}
