package com.i108.conferenceapp.activities;

import com.i108.conferenceapp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {

	private static int SPLASH_TIME_OUT = 1500;
	private Handler handler;
	private Runnable starter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		handler = new Handler();
		starter = new Runnable() {
			@Override
			public void run() {
				Intent i = new Intent(SplashActivity.this, MainActivity.class);
				startActivity(i);

				finish();
			}
		};
		handler.postDelayed(starter, SPLASH_TIME_OUT);
	}
	
	@Override
	public void onBackPressed() {
		handler.removeCallbacks(starter);
		super.onBackPressed();
	}
	
}
