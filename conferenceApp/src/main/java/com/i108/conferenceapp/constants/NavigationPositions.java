package com.i108.conferenceapp.constants;

public class NavigationPositions {

	public static final int NEWS_RSS = 1;
	public static final int TWITTER_FEED = 2;
	public static final int AGENDA = 3;
	public static final int MY_PLAN = 4;
	public static final int SPEAKERS = 5;
	public static final int SPONSORS = 6;
	public static final int LOCATIONS = 7;
	public static final int SCAN_QR_CODE = 8;
	public static final int ABOUT_CONFERENCE = 9;

}
