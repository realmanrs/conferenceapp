package com.i108.conferenceapp;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.i108.conferenceapp.database.DatabaseManager;
import com.i108.conferenceapp.utils.ImagesManager;

public class ConferenceApp extends Application {

	private DatabaseManager dbManager;
	private ImagesManager imagesManager;

	@Override
	public void onCreate() {
		super.onCreate();
		dbManager = new DatabaseManager(this);
		imagesManager = new ImagesManager(this);
	}

	public DatabaseManager getDbManager() {
		return dbManager;
	}

	public ImagesManager getImagesManager() {
		return imagesManager;
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}
