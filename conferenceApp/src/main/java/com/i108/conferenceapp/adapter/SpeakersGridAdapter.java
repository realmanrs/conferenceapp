package com.i108.conferenceapp.adapter;

import java.util.List;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.ConferenceApp;
import com.i108.conferenceapp.model.Speaker;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpeakersGridAdapter extends ArrayAdapter<Speaker> {

	private Context context;
	private int layoutId;
	private List<Speaker> data;
	private ConferenceApp app;

	public SpeakersGridAdapter(Context context, List<Speaker> data) {
		super(context, R.layout.item_speaker, data);
		this.context = context;
		this.data = data;
		this.layoutId = R.layout.item_speaker;
		this.app = (ConferenceApp) ((Activity) context).getApplication();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;
		final Speaker speaker = data.get(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutId, parent, false);

			holder = new ViewHolder();
			holder.ivPhoto = (ImageView) row.findViewById(R.id.iv_photo);
			holder.tvName = (TextView) row.findViewById(R.id.tv_name);
			holder.tvCompany = (TextView) row.findViewById(R.id.tv_company);

			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		app.getImagesManager().loadImage(speaker.getPhoto().getFilename(), holder.ivPhoto);
		holder.tvName.setText(speaker.getName() + " " + speaker.getSurname());
		holder.tvCompany.setText(speaker.getCompany());

		return row;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public long getItemId(int position) {
		return data.get(position).getId();
	}

	private class ViewHolder {
		ImageView ivPhoto;
		TextView tvName;
		TextView tvCompany;
	}

}
