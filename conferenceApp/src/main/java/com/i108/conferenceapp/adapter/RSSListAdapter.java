package com.i108.conferenceapp.adapter;

import java.util.List;

import org.mcsoxford.rss.RSSItem;

import com.i108.conferenceapp.R;
import com.i108.conferenceapp.utils.Utils;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import android.app.Activity;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RSSListAdapter extends ArrayAdapter<RSSItem> {

	private Context context;
	private List<RSSItem> data;
	private int layoutId;
	private SparseBooleanArray collapsedContent;

	public RSSListAdapter(Context context, List<RSSItem> data) {
		super(context, R.layout.item_rss_news, data);
		this.context = context;
		this.data = data;
		this.layoutId = R.layout.item_rss_news;
		this.collapsedContent = new SparseBooleanArray(data.size());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutId, parent, false);

			holder = new ViewHolder();
			holder.tvDate = (TextView) row.findViewById(R.id.tv_date);
			holder.tvTitle = (TextView) row.findViewById(R.id.tv_title);
			holder.tvContent = (ExpandableTextView) row.findViewById(R.id.tv_content);

			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		holder.tvDate.setText(Utils.dateToString(data.get(position).getPubDate()));
		holder.tvTitle.setText(data.get(position).getTitle());
		holder.tvContent.setText(data.get(position).getDescription(), collapsedContent, position);

		return row;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	private class ViewHolder {
		TextView tvDate;
		TextView tvTitle;
		ExpandableTextView tvContent;
	}

}
