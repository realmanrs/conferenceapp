package com.i108.conferenceapp.model;

import java.util.Calendar;
import java.util.Date;

import com.i108.conferenceapp.utils.Utils;

public class DayTag {

	protected Calendar calendar;

	public DayTag(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
	}

	public String getId() {
		return Utils.dateToString(calendar.getTime());
	}

	public String getTitle() {
		return calendar.get(Calendar.DAY_OF_MONTH) + "."
				+ String.format("%02d", calendar.get(Calendar.MONTH) + 1) + ".";
	}

	public int getDayOfWeek() {
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

}
