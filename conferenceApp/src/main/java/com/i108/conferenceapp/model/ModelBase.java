package com.i108.conferenceapp.model;

public abstract class ModelBase {
	
	protected long id;
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

}
