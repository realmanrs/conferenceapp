package com.i108.conferenceapp.model;

public class Photo extends ModelBase {
	
	protected String filename;
	
	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}

}
