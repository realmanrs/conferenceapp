package com.i108.conferenceapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;

public class Utils {

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
	private static final String TIME_FORMAT = "HH:mm";
	private static final String DAY_FORMAT = "dd.MM";

	public static String dateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
		return sdf.format(date);
	}

	public static Date stringToDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String getTime(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
		return sdf.format(date);
	}

	public static String getDay(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DAY_FORMAT, Locale.getDefault());
		return sdf.format(date);
	}

	public static int convertDpToPixels(Context context, int dp) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		int px = (int) (dp * (metrics.densityDpi / 160f));
		return px;
	}

	public static void openUrl(Context context, String url) {
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		context.startActivity(intent);
	}

}
